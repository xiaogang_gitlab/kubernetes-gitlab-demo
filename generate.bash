#!/bin/bash

set -e

scope_files() {
  for file in "$@"; do
    cat "$file"
    echo "---"
  done
}

scope_files \
  gitlab/redis-* \
  gitlab/postgresql-* \
  gitlab/gitlab-* \
  gitlab-runner/gitlab-runner-* \
  > gitlab-full.yml

scope_files \
  load-balancer/lego/* \
  load-balancer/nginx/* \
  > load-balancer-full.yml

scope_files ingress/* \
  > gitlab-ingress.yml

if [ -z "$NGROK_HOSTNAME" ]; then
# for regular kubernetes
scope_files \
  load-balancer/lego/* \
  load-balancer/nginx/* \
  gitlab-ns.yml \
  gitlab-config.yml \
  gke/storage.yml \
  gitlab/redis-* \
  gitlab/postgresql-* \
  gitlab/gitlab-* \
  gitlab-runner/gitlab-runner-* \
  ingress/* \
  > gitlab-all.yml
else
# for minikube
scope_files \
  load-balancer/lego/* \
  load-balancer/nginx/* \
  gitlab-ns.yml \
  gitlab-config.yml \
  gke/storage.yml \
  gitlab/redis-* \
  gitlab/postgresql-* \
  gitlab/gitlab-* \
  gitlab-runner/gitlab-runner-* \
  ingress/* \
  minikube-dns.yml \
  > gitlab-all.yml
fi



if [[ -z "$GITLAB_GKE_IP" || -z "$GITLAB_GKE_DOMAIN" || -z "$GITLAB_LEGO_EMAIL" ]]; then
    echo "A needed variable is not exported. Check that GITLAB_GKE_IP, GITLAB_GKE_DOMAIN, and GITLAB_LEGO_EMAIL environment variables are set. Cannot replace & rename."
    exit 1
fi

dashed_domain="${GITLAB_GKE_DOMAIN//./-}"
echo "Using gitlab-${dashed_domain}.yml"
cp gitlab-all.yml gitlab-${dashed_domain}.yml

sed -i.bak "
    s/@GITLAB_GKE_IP@/${GITLAB_GKE_IP}/g
    s/@GITLAB_GKE_DOMAIN@/${GITLAB_GKE_DOMAIN}/g
    s/@GITLAB_LEGO_EMAIL@/${GITLAB_LEGO_EMAIL}/g
" gitlab-${dashed_domain}.yml

# for minikube
if [ -n "$NGROK_HOSTNAME" ]; then
   sed -i -e "
           s/@NGROK_HOSTNAME@/$NGROK_HOSTNAME/g
           s;volume.beta.kubernetes.io/storage-class.*$;volume.beta.kubernetes.io/storage-class: standard;g
           s/registry\.$GITLAB_GKE_DOMAIN/$NGROK_HOSTNAME/g 
           s/ external_scheme:.*$/ external_scheme: http/g
           s/ mattermost_external_scheme:.*$/ mattermost_external_scheme: http/g
"          gitlab-${dashed_domain}.yml
fi

rm gitlab-${dashed_domain}.yml.bak

